Documentation for Falcon FKlab Extensions
=========================================

Falcon is a software for real-time processing of neural signals to enable
short-latency closed-loop feedback in experiments that try to causally link
neural activity to behavior. Example use cases are the detection of hippocampal
ripple oscillations or online decoding and detection of hippocampal replay
patterns.

This extension does not work as standalone and needs to be integrated with Falcon [add readthedoc falcon link]


.. toctree::
   :maxdepth: 2
   :glob:

   overview
   datatypes
   processors
   hardware
   libs
   tools
   example
   resource


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

