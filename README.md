![Falcon version](https://img.shields.io/badge/Falcon-v1.0.0-blue)

# Falcon decoding extensions

The extension does not work as standalone and needs to be integrated with Falcon [https://falcon-core.readthedocs.io/en/latest/]

The full documentation can be found here - [add readthedoc extension link] but below, you can found a quick overview :

**Processor** : 

**Datatype** :



**Additional libraries**:

- fklab-compressed-decoder


## Contribution 

If your issue concerned processors, datatypes or libs included in this repository, don't hesitate to add an issue 
describing the problem / or the feature to develop. Add the graph (+ eventually the config file) used to run Falcon
is highly recommended. 
 
To develop a new extension, an issue can be open in here for guidance but most probably the maintainer will advise you to 
create your own repository and then open an PR in Falcon to link your extension doc in the Falcon doc. 
